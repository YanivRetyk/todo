<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('books')->insert([
[    'author' => 'Yuval noah harari',
    'title' => 'The Histyory of tomaroow',
    'created_at' => date('Y-m-d G:i:s'),
    'user_id' => 1,
       ],
[    'author' => '12 rulles of life',
    'title' => 'Jordan peaterson',
    'created_at' => date('Y-m-d G:i:s'),
    'user_id' => 2,
],
[   'author' => 'aye pluto',
    'title' => 'Bar yron',
    'created_at' => date('Y-m-d G:i:s'),
    'user_id' => 3,
    ],
    [    'author' => 'what did happin',
    'title' => 'anonimus',
    'created_at' => date('Y-m-d G:i:s'),
    'user_id' => 4,
    ],
[    'author' => 'noylan',
    'title' => 'Eshcol Nevo',
    'created_at' => date('Y-m-d G:i:s'),
    'user_id' => 5,
    ],
    ]);
    }
}
